package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import hust.soict.globalict.aims.Playable;

public class CompactDisc extends Disc implements Playable{
	private String artist;
	private ArrayList<Track> tracks = new ArrayList<Track>();
	
	public String getArtist() {
        return artist;
    }
	
	public CompactDisc(String title){
        super(title);
    }
    public CompactDisc(String title, String category){
        super(title,category);
    }
    public CompactDisc(String title,String category, String director, float cost,int length,String artist){
        super(title, category, director, cost, length);
        this.artist=artist;
    }
    
	public void addTrack(Track track) {
		 if (tracks != null && tracks.contains(track)) {
	            System.out.println("The track had already exsited.");
	            return;
	        }

	        tracks.add(track);
	        System.out.println("The track was added.");

	}
    public void removeTrack(Track track) {
    	if (!tracks.contains(track)) {
            System.out.println("The track does not exist in tracks");
            return;
        }
        tracks.remove(track);
        System.out.println("The Track was removed.");
	}
    
    public int getLength(){
        int sum=0;
        for(Track track : tracks)
        	sum=sum+ track.getLength();
        return sum;
    }
	
    public void play(){
        System.out.println("Playing: "+ this.getTitle());
        System.out.println("Length of CD: "+ getLength());
        for(Track i : tracks)i.play();
    }
}
