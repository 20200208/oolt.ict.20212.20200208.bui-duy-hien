package hust.soict.globalict.aims;
import java.util.Scanner;

import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.order.Order;


public class DiskTest {
	public static void main(String [] args){
		Order anOrder = new Order();
        DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
        dvd1.setCategory("Animation");
        dvd1.setCost(19.95f);
        dvd1.setDirector("Roger Allers");
        dvd1.setLength(87);

        DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
        dvd2.setCategory("Science Fiction");
        dvd2.setCost(24.95f);
        dvd2.setDirector("George Lucas");
        dvd2.setLength(124);

        DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladin");
        dvd3.setCategory("Animation");
        dvd3.setCost(18.99f);
        dvd3.setDirector("John Musker");
        dvd3.setLength(90);

        DigitalVideoDisc[] dvdList = new DigitalVideoDisc[3];
        dvdList[0] = dvd1;
        dvdList[1] = dvd2;
        dvdList[2] = dvd3;
//        anOrder.addDigitalVideoDisc(dvdList);
//        
//        Scanner sc = new Scanner(System.in);
//        System.out.print("Enter title: ");
//        String title = sc.nextLine();
//        System.out.println(dvd1.search(title));
//        System.out.println(anOrder.totalCost());
//        DigitalVideoDisc luckyItem=anOrder.getALuckyItem();
//        System.out.println("You've just got "+luckyItem.getTitle());
//        anOrder.printOrder();
//        sc.close();
	}
}
