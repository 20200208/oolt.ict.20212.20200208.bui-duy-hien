package hust.soict.globalict.lab01;
import javax.swing.JOptionPane;
public class CalculateTwoNumbers {
	public static void main(String[] args){
        String strNum1, strNum2;
        String strNote="You've just enter: ";
        strNum1=JOptionPane.showInputDialog(null,"Please input the first number: ","Input the first number",JOptionPane.INFORMATION_MESSAGE );
        strNote+=strNum1+ " and ";
        double num1 = Double.parseDouble(strNum1);
        strNum2=JOptionPane.showInputDialog(null,"Please input the second number: ","Input the second number",JOptionPane.INFORMATION_MESSAGE );
        strNote+=strNum2;
        double num2 = Double.parseDouble(strNum2);
        JOptionPane.showMessageDialog(null, strNote+
        "\nThe sum of two numbers: "+(num1+num2)+
        "\nThe differnce of two numbers: "+(num1-num2)+
        "\nThe product of two numbers: "+(num1*num2)+
        "\nThe quotitent of two numbers: "+(num1/num2),"Result",JOptionPane.INFORMATION_MESSAGE);
        System.exit(0);
    }
}
