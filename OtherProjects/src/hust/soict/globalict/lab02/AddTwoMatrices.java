package hust.soict.globalict.lab02;
import java.util.Scanner;
public class AddTwoMatrices {
	public static void main(String[] args) {
		int r,c;
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the number of rows: ");
		r = sc.nextInt();
		System.out.println("Enter the number of columns: ");
		c = sc.nextInt();
	    float a[][]= new float[r][c];
	    float b[][]= new float[r][c];
	    float sum[][]= new float[r][c];
	    System.out.println("Enter elements of matrix 1: ");
	    for (int i =0;i<r;i++)  
	    {  
	        for(int j=0;j<c;j++)  
	        {  
	            System.out.print("Enter element a["+i+"]"+"["+j+"]: ");  
	            a[i][j]=sc.nextFloat();  
	        }
	    }
	    System.out.println("Enter elements of matrix 2: ");
	    for (int i =0;i<r;i++)  
	    {  
	        for(int j=0;j<c;j++)  
	        {  
	            System.out.print("Enter element b["+i+"]"+"["+j+"]: ");  
	            b[i][j]=sc.nextFloat();  
	        }
	    }
	    System.out.println("Sum of two matrices is: ");
	    for (int i =0;i<r;i++) {   
	        for(int j=0;j<c;j++) {  
	        	sum[i][j]=a[i][j]+b[i][j]; 
	        	System.out.print(sum[i][j]+" ");
	        }
	        System.out.println();
	    }
	    sc.close();
	}
}
