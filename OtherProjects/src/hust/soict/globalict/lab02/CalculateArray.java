package hust.soict.globalict.lab02;
import java.util.Scanner;
public class CalculateArray {
	public static void main(String[] args) {
		float temp;
		float s=0;
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter lenghth of array: ");
		int n= sc.nextInt();
		float a[]=new float [n];
		System.out.println("Enter array: ");
		for(int i=0; i<n; i++)
			a[i]=sc.nextFloat();
        System.out.println("Original array: ");
        for(int i=0; i<n; i++) {
        	s+=a[i];
        	System.out.println(a[i]+" ");
        }	
        for(int i=0; i<n-1; i++) {
        	for(int j=i+1; j< n; j++) {
        		if(a[i]>a[j]) {
        			temp=a[i];
        			a[i]=a[j];
        			a[j]= temp;        			
        		}
        	}
        }
        System.out.println("Array sorted: ");
        for(int i=0; i<n; i++)
        	System.out.println(a[i]+" ");
        System.out.println("The sum: "+s);
        System.out.println("The average: "+(s/n));
        sc.close();
	}
}
  